package main.Weather;

import main.WeatherException.GoogleWeatherAPIException;
import main.WeatherException.WeatherException;

/**
 * Created by ThinkPad on 25.5.2017 г..
 */
public class Weather {

    public Data getWeather(Location locatioan) throws WeatherException
    {
        GoogleWeatherProvider weather=new GoogleWeatherProvider(locatioan);

        try{
            GoogleResult result=new GoogleResult(weather.getData());

            return result.getData();
        }catch (GoogleWeatherAPIException e)
        {
            WeatherException f=new WeatherException(e);
            throw f;
        }

    }

}
