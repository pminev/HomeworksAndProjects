package main.Weather;

/**
 * Created by ThinkPad on 25.5.2017 г..
 */
public class GoogleWeatherProvider {
    private Data data;

    public GoogleWeatherProvider()
    {
        data=null;
    }

    public GoogleWeatherProvider(Location location)
    {
        data=location.getData();
    }

    public Data getData()
    {
        return new Data(data);
    }

}
