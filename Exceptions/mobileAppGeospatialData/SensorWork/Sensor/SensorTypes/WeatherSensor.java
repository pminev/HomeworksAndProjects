package SensorWork.Sensor.SensorTypes;

import SensorWork.Data.SensorData;
import SensorWork.Exception.SensorException;
import SensorWork.Exception.SensorReachableException;
import SensorWork.Sensor.Sensor;

/**
 * Created by ThinkPad on 25.5.2017 г..
 */
public class WeatherSensor extends Sensor {
    private String feeling;
    private int temperature;

    public WeatherSensor(){
    }

    public WeatherSensor(WeatherSensor sensor)
    {

        super(sensor);
            this.feeling=new String(sensor.feeling);
            this.temperature=sensor.temperature;
    }


    public void setFeeling(String feeling) {
        this.feeling = new String(feeling);
    }

    public void setTemperature(int temperature) {
        this.temperature = temperature;
    }

    public String getFeeling() {
        return new String(feeling);
    }

    public int getTemperature() {
        return temperature;
    }

    public Sensor clone()
    {
        return new WeatherSensor(this);
    }

    @Override
    public SensorData getData(boolean net) throws SensorReachableException {
        if (!net)
            throw new SensorReachableException("Connection Error!");

        SensorData data=new SensorData();
        data.setPrimaryData(location,descriptionData,name);
        data.setTemperature(temperature);
        data.setFeeling(feeling);

        return data;
    }
}
