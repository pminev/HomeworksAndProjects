package Streams.ReadWriteFile;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

/**
 * Created by ThinkPad on 1.6.2017 г..
 */
public final class Write {

    public static void write(File file, String content) throws Exception{
        String[] separated=file.getName().split(".");
        StringBuilder newFileName =new StringBuilder();

        for (int i = 0; i < separated.length-1; i++) {
            newFileName.append(separated[i]);
        }
        newFileName.append("_copy.");
        newFileName.append(separated[separated.length-1]);

        try (BufferedWriter bufferedWriter = new BufferedWriter(new FileWriter(newFileName.toString()))) {
            bufferedWriter.write(content);
        } catch (IOException e)
        {
            e.printStackTrace();
        }

    }
}
