package main.WeatherException;

import main.Weather.Weather;

/**
 * Created by ThinkPad on 25.5.2017 г..
 */
public class WeatherException extends Exception {

    public WeatherException(String s)
    {
        super(s);
    }

    public WeatherException(String message,Throwable caouse) {
        super(message,caouse);
    }

    public WeatherException(Throwable t)
    {
        super(t);
    }

    public String getMessage()
    {
        return super.getMessage();
    }
}
