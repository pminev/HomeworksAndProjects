package SensorWork.Stream;

import SensorWork.Data.SensorData;
import SensorWork.Exception.SensorReachableException;
import SensorWork.Sensor.Sensor;
import sun.plugin2.gluegen.runtime.StructAccessor;

/**
 * Created by ThinkPad on 25.5.2017 г..
 */
public class Stream {
    private Sensor sensor;

    public Stream()
    {
        sensor=null;
    }

    public Stream(Sensor sensor)
    {
        this.sensor=sensor.clone();
    }

    public Stream(Stream stream){
        this.sensor=stream.sensor.clone();
    }

    public void open(Sensor sensor)
    {
        this.sensor=sensor.clone();
    }

    public SensorData read(boolean isConnected) throws SensorReachableException
    {
        if (!isConnected)
            throw new SensorReachableException("Connection Error!");
        return sensor.getData(isConnected);
    }

    public void close()
    {
        sensor=null;
    }


}
