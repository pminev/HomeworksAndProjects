package SensorWork.Sensor;
import SensorWork.Data.SensorData;
import SensorWork.Exception.SensorException;
import SensorWork.Exception.SensorReachableException;

/**
 * Created by ThinkPad on 25.5.2017 г..
 */
public abstract class Sensor {
    protected Location location;
    protected String name;
    protected String descriptionData;


    public Sensor(){
        location=null;
        name=null;
        descriptionData=null;
    }

    public Sensor(Sensor other)
    {
        location=new Location(other.location);
        name=new String(other.name);
        descriptionData=new String(other.descriptionData);
    }

    public void registerSensor(String name, String location, String descr, boolean net) throws SensorReachableException
    {
            if (!net)
                new SensorReachableException("No connection!");

            this.location=new Location(location);
            this.name=new String(name);
            this.descriptionData=new String(descr);

    }

    private void setName(String name,boolean net) throws SensorReachableException
    {
        if (!net)
            throw new SensorReachableException("Not reachable");

        this.name=new String(name);
    }

    public SensorData getData(boolean net) throws SensorReachableException
    {
        if (!net)
            throw new SensorReachableException("No connection");

        return new SensorData();
    }

    public Sensor clone()
    {
        return null;
    }


}
