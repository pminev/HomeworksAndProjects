package main.Weather;

/**
 * Created by ThinkPad on 25.5.2017 г..
 */
public class Data {
    private int degrees;
    private String howItIsFeeling;

    public Data(int degrees, String str)
    {
        this.degrees=degrees;
        howItIsFeeling=new String(str);
    }

    public Data(Data dat)
    {
        degrees=dat.degrees;
        howItIsFeeling=new String(dat.howItIsFeeling);
    }

}
