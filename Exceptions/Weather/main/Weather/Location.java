package main.Weather;

/**
 * Created by ThinkPad on 25.5.2017 г..
 */
public class Location {
    private String city;
    private String country;
    private Data data;

    public Location(String city,String country)
    {
        this.city=new String(city);
        this.country=new String(country);
    }

    public Location(Location location)
    {
        this.city=new String(location.city);
        this.country=new String(location.country);
    }

    public Data getData()
    {
        return new Data(data);
    }

    public void setData(Data data)
    {
        this.data=new Data(data);
    }
}
