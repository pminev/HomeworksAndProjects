package SensorWork.Sensor.SensorTypes;

import SensorWork.Data.SensorData;
import SensorWork.Exception.SensorReachableException;
import SensorWork.Sensor.Sensor;

/**
 * Created by ThinkPad on 25.5.2017 г..
 */
public class WaterSensor extends Sensor {
    private int depth;
    private int temperature;

    public WaterSensor(WaterSensor sensor)
    {
        depth=sensor.depth;
        temperature=sensor.temperature;
    }

    public Sensor clone()
    {
        return new WaterSensor(this);
    }

    public void setDepth(int depth) {
        this.depth = depth;
    }

    public void setTemperature(int temperature) {
        this.temperature = temperature;
    }

    public int getDepth() {
        return depth;
    }

    public int getTemperature() {
        return temperature;
    }

    @Override
    public SensorData getData(boolean net) throws SensorReachableException {
        if (!net)
            throw new SensorReachableException("Connection Error!");

        SensorData data=new SensorData();
        data.setPrimaryData(location,descriptionData,name);
        data.setDepth(depth);
        data.setTemperature(temperature);

        return data;
    }


}
