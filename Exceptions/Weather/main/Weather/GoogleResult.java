package main.Weather;

import main.WeatherException.GoogleWeatherAPIException;
import main.WeatherException.WeatherException;

/**
 * Created by ThinkPad on 25.5.2017 г..
 */
public class GoogleResult {
    private Data data;

    public GoogleResult(Data dat) throws GoogleWeatherAPIException
    {
        if (dat==null)
            throw new GoogleWeatherAPIException("Data is null");

        data=new Data(dat);
    }

    public GoogleResult(int deg,String feeling) throws GoogleWeatherAPIException
    {
        if (deg>100)
            throw  new GoogleWeatherAPIException("Bad deg parameter!");
        data=new Data(deg,feeling);
    }

    public Data getData() throws GoogleWeatherAPIException
    {
        return new Data(data);
    }
}
