package SensorWork.Sensor.SensorTypes;

import SensorWork.Data.SensorData;
import SensorWork.Exception.SensorReachableException;
import SensorWork.Sensor.Sensor;

/**
 * Created by ThinkPad on 25.5.2017 г..
 */
public class AirSensor extends Sensor {
    private int humidity;
    private int pressure;
    private int windSpeed;

    public AirSensor(){
    }

    public AirSensor(AirSensor sensor)
    {
        super(sensor);
        humidity=sensor.humidity;
        pressure=sensor.humidity;
        windSpeed=sensor.windSpeed;
    }

    public void setHumidity(int humidity) {
        this.humidity = humidity;
    }

    public void setPressure(int pressure) {
        this.pressure = pressure;
    }

    public void setWindSpeed(int windSpeed) {
        this.windSpeed = windSpeed;
    }

    public int getHumidity() {
        return humidity;
    }

    public int getPressure() {
        return pressure;
    }

    public int getWindSpeed() {
        return windSpeed;
    }

    public Sensor clone()
    {
        return new AirSensor(this);
    }

    @Override
    public SensorData getData(boolean net) throws SensorReachableException {
        if (!net)
            throw new SensorReachableException("Connection Error!");

        SensorData data=new SensorData();
        data.setPrimaryData(location,descriptionData,name);
        data.setHumidity(humidity);
        data.setPressure(pressure);
        data.setWindSpeed(windSpeed);

        return data;
    }


}
