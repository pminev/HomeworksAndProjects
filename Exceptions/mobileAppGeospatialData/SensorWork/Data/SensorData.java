package SensorWork.Data;

import SensorWork.Exception.SensorException;
import SensorWork.Exception.SensorReachableException;
import SensorWork.Sensor.Location;

/**
 * Created by ThinkPad on 25.5.2017 г..
 */
public class SensorData {
    private Location location;
    private String name;
    private String descriptionData;

    private int humidity;
    private int pressure;
    private int windSpeed;

    private int depth;
    private int temperature;

    private String feeling;

    public SensorData() {
    }

    public void setPrimaryData(Location location,String descriptionData,String name){
        this.location=new Location(location);
        this.descriptionData=new String(descriptionData);
        this.name=new String(name);
    }

    public void setHumidity(int humidity) {
        this.humidity = humidity;
    }

    public void setPressure(int pressure) {
        this.pressure = pressure;
    }

    public void setWindSpeed(int windSpeed) {
        this.windSpeed = windSpeed;
    }

    public void setDepth(int depth) {
        this.depth = depth;
    }

    public void setTemperature(int temperature) {
        this.temperature = temperature;
    }

    public void setFeeling(String feeling) {
        this.feeling = new String(feeling);
    }

    public Location getLocation() {
        return new Location(location);
    }

    public String getName() {
        return new String(name);
    }

    public String getDescriptionData() {
        return new String(descriptionData);
    }

    public int getHumidity() {
        return humidity;
    }

    public int getPressure() {
        return pressure;
    }

    public int getWindSpeed() {
        return windSpeed;
    }

    public int getDepth() {
        return depth;
    }

    public int getTemperature() {
        return temperature;
    }

    public String getFeeling() {
        return new String(feeling);
    }
}
