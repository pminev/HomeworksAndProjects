package SensorWork.Exception;

/**
 * Created by ThinkPad on 25.5.2017 г..
 */
public class SensorReachableException  extends SensorException{
    public SensorReachableException(String s)
    {
        super(s);
    }

    public SensorReachableException(String s,Throwable t)
    {
        super(s,t);
    }
}
