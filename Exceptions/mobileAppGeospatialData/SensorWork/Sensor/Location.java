package SensorWork.Sensor;

/**
 * Created by ThinkPad on 25.5.2017 г..
 */
public class Location {
    private String country;

    public Location()
    {
        country=null;
    }

    public Location(String country)
    {
        this.country=new String(country);
    }

    public Location(Location loc)
    {
        country= new String(loc.country);
    }

    public String getCountry()
    {
        return new String(country);
    }
}
