package Streams.ReadWriteFile;

import java.io.*;

/**
 * Created by ThinkPad on 1.6.2017 г..
 */
public final class Read {

    public static String read(File file) throws Exception
    {
        StringBuilder builder=new StringBuilder();
        try(BufferedReader br=new BufferedReader(new FileReader(file)))
        {

            String string;
            while ((string=br.readLine())!=null)
            {
                builder.append(string);
                builder.append(System.lineSeparator());
            }
            builder.setLength(builder.length()-1);

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

        return builder.toString();
    }
}
