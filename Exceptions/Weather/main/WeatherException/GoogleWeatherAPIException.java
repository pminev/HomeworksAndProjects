package main.WeatherException;

/**
 * Created by ThinkPad on 25.5.2017 г..
 */
public class GoogleWeatherAPIException extends Exception{
    public GoogleWeatherAPIException(String s)
    {
        super(s);
    }

    public GoogleWeatherAPIException(String s,Throwable cause)
    {
        super(s,cause);
    }

}
