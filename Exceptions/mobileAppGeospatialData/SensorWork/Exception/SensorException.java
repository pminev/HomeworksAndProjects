package SensorWork.Exception;

/**
 * Created by ThinkPad on 25.5.2017 г..
 */
public class SensorException extends Exception{
    public SensorException(String s)
    {
        super(s);
    }

    public SensorException(String s,Throwable cause)
    {
        super(s,cause);
    }

    public SensorException(Throwable s)
    {
        super(s);
    }
}
