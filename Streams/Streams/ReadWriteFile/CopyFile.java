package Streams.ReadWriteFile;

import java.io.File;

/**
 * Created by ThinkPad on 1.6.2017 г..
 */
public final class CopyFile {

    public static void copyFile(String path)
    {
        File file=new File(path);
        try{
            String fileContent=Read.read(file);
            Write.write(file,fileContent);
        } catch (Exception e) {
            e.printStackTrace();
        }

    }
}
